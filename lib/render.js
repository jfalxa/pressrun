import array from 'array-flatten'

const EMPTY_TAGS = [
  'area',
  'base',
  'br',
  'col',
  'embed',
  'hr',
  'img',
  'input',
  'keygen',
  'link',
  'meta',
  'param',
  'source',
  'track',
  'wbr',
]

// check if the given value is a valid child element
function isChild(value) {
  return (
    typeof value === 'string' ||
    typeof value === 'number' ||
    value instanceof Promise ||
    Array.isArray(value)
  )
}

// convert an object into a string of attributes of a dom element
function htmlAttrs(attributes) {
  if (!attributes) return ''

  const attrs = Object.keys(attributes)
    .map((a) => `${a}="${attributes[a]}"`)
    .join(' ')

  return ` ${attrs}`
}

// create a promise that resolves when all children and their own children are resolved
function resolveChildren(children) {
  const resolving = children.map((child) =>
    Promise.resolve(child).then((result) =>
      Array.isArray(result) ? resolveChildren(result) : result
    )
  )

  return Promise.all(resolving).then(array.flatten)
}

// asyncly generate html from the given parameters
export default function h(tag, attrs, ...children) {
  if (EMPTY_TAGS.includes(tag)) {
    return `<${tag}${htmlAttrs(attrs)}>`
  }

  if (isChild(attrs)) {
    children.unshift(attrs)
    attrs = null
  }

  return resolveChildren(children).then(
    (result) => `<${tag}${htmlAttrs(attrs)}>${result.join('')}</${tag}>`
  )
}

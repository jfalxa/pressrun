import pt from 'path'
import fs from 'fs/promises'

let queue = new Set()

export function asset(file) {
  queue.add(file)
  return `/assets/${file}`
}

export async function makeAssets(source, target) {
  for (const file of queue) {
    const original = pt.resolve(source, file)
    const copy = pt.resolve(target, 'assets', file)

    await fs
      .mkdir(pt.dirname(copy), { recursive: true })
      .catch((e) => console.log('Error creating path to asset', e))

    await fs
      .copyFile(original, copy)
      .catch((e) => console.log('Error copying asset', e))
  }

  queue.clear()
}

import pt from 'path'
import fs from 'fs/promises'
import fg from 'fast-glob'

// clear and create target directory
export async function makeTarget(target) {
  const path = pt.resolve(target)

  await fs
    .rmdir(pt.join(path), { recursive: true })
    .catch((e) => console.log('Error while clearing target directory', e))

  await fs
    .mkdir(pt.join(path), { recursive: true })
    .catch((e) => console.log('Error while creating target directory', e))
}

// list pages present in source directory
export function listPages(source) {
  const pages = pt.resolve(source, 'pages', '**/*.js')
  return fg(pages).catch((e) => console.log('Error while listing pages', e))
}

// create a html file for the given page
async function makeHTML(target, route, content) {
  const path = pt.resolve(target, route).replace(/(.js)?$/, '.html')
  const data = await content

  await fs
    .mkdir(pt.dirname(path), { recursive: true })
    .catch((e) => console.log('Error creating path to page', e))

  await fs
    .writeFile(path, `<!DOCTYPE html>${data}`)
    .catch((e) => console.log('Error creating page', e))
}

// create html files for each route
export async function makeRoutes(target, route, content) {
  const dir = pt.dirname(route)
  const routes = await content

  // build one page per route
  for (const index in routes) {
    const subroute = pt.join(dir, index)
    const subcontent = routes[index]

    await makeHTML(target, subroute, subcontent)
  }
}

// generate files from given page
export async function makePage(page, source, target) {
  const content = import(page).then((module) => module.default)
  const route = pt.relative(pt.resolve(source, 'pages'), page)

  if (pt.basename(route) === 'routes.js') {
    await makeRoutes(target, route, content)
  } else {
    await makeHTML(target, route, content)
  }
}

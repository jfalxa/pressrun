#!/usr/bin/env node
import { makeAssets } from './lib/assets.js'
import { makeTarget, listPages, makePage } from './lib/build.js'

export { asset } from './lib/assets.js'

async function main(source = 'src', target = 'public') {
  await makeTarget(target)

  const pages = await listPages(source)

  for (const page of pages) {
    await makePage(page, source, target)
  }

  await makeAssets(source, target)
}

main(...process.argv.slice(2))

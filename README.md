# pressrun

Generate static HTML pages from idiomatic JS.

## Installation

`npm install pressrun`

And inside your project's `package.json`, make sure you specify `"type": "module"`.

## Usage

`pressrun` works by giving you a set of functions that, when used in conjunction with the provided binary, will allow you to generate static HTML files.

### CLI

`pressrun [source] [target]`

This will look for a `pages/` directory inside the specified `source` (default: `src/`) and then for each js file it finds inside, it will generate a corresponding html file (following the same path) inside the `target` directory (default: `public/`).

An exception to that rule would be if the builder finds files named `routes.js`. More on that later.

### Implementation

`pressrun` exports a set of functions mirroring HTML elements that return a view of one element. You can import them from `pressrun/html.js` and compose them to describe a more complex view. Every file inside your `pages/` folder shoud `export default` a variable built with these functions in order to work.

These helpers are all made by binding the `h` function (also exported in this file) to a tag name. `h` has the following signature:

```
h(tag: string, attributes: object, ...children: elements[]) -> view: Promise
```

N.B.: the `attributes` argument can be omitted and replaced by a child.

#### Example:

Write the following code:

<!-- prettier-ignore -->
```js
// src/pages/index.js
import { html, head, meta, title, body, h1 } from 'pressrun/html.js'

const Page =
  html({ lang: 'en' },
    head(
      meta({ charset: 'UTF-8' }),
      title('My Page'),
    ),

    body(
      h1({ class: 'title' }, 'Hello')
    )
  )

export default Page
```

Then run `npx pressrun`.

You should now see a `public` folder with an `index.html` file inside with this markup:

<!-- prettier-ignore -->
```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>My Page</title>
  </head>
  <body>
    <h1 class="title">Hello</h1>
  </body>
</html>
```

## Async rendering

Every element built with the `h` function (and everything based on it) actually generates an HTML string asynchronously. This means that any element inside your view can be a promise, and so eventually you can build a list after an api call or any other async process. To render something, the promise should resolve to view object.

<!-- prettier-ignore -->
```js
import { ul, li } from 'pressrun'
import { fetchProjects } from './api.js'

const ProjectList = 
  ul({ class: 'project-list' },
    fetchProjects().then(projects => 
      projects.map(project => (
        li({ class: 'project' },
          a({ href: `project/${project.id}` }, project.name)
        )
      ))
    )
  )
```

## Dynamic pages

Some pages with repetitive content can be dynamically generated for easier development.
To do so, you need to create a `routes.js` file. You can have one of them per folder inside `pages/` (including `pages/`).

These files should `export default` a regular JS object (or a promise resolving to an object), with its keys being the route relative to the directory where the current `routes.js` is placed, and its values should be a regular view object, the same kind you would normally `export default` in other pages.

<!-- prettier-ignore -->
```js
// src/pages/projects/routes.js
import { div, h1, p } from 'pressrun/html.js'
import { fetchProjects } from './api.js'

const Project = project => 
  div({ class: 'project' },
    h1({ class: 'project-name' }, project.name),
    p({ class: 'project-description' }, project.description)
  )

// create one route per project
export default fetchProjects().then(projects => {
  const routes = {}

  for (const project of projects) {
    routes[project.id] = Project(project)
  }

  return routes
})
```

In this example, we create a `routes.js` file inside the `pages/projects/` folder.

We start by creating a `Project` function that will generate a view for a given project. The fact that we're only using JS functions allows us to be creative in how we create and compose our views.

Then, we call `fetchProjects()` to get a list of projects and we build an object by giving it project ids as keys and project views as values.

Now, `pressrun` will be able to generate one page per project and place it inside `public/projects/`.

## Assets

You can specify assets that should be included in your static site by using the exported `asset` function. This will tell `pressrun` to copy the file to its target folder (`public`) so it can be loaded by the browser when the site is read.

```
asset(path: string) -> publicPath: string
```

The `asset` function takes the path to the asset relative to the source folder (`src/`) given to `pressrun` and returns the path of the copied asset relative to the target folder (`public/`).

<!-- prettier-ignore -->
```js
import { asset } from 'pressrun'
import { div, img, span } from 'pressrun/html.js'

const Figure = 
  div({ class: 'figure' },
    img({ src: asset('images/figure-1.png') }),
    span('Figure #1')
  )

export default Figure
```

When generating HTML from this page, `pressrun` will create the folder `public/assets/images/` and copy `src/images/figure-1.png` inside.
